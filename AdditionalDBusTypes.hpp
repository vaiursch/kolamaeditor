/* 
 * kolamaEditor - Editor for simple animations on KoLaMa
 * 
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 * 
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#ifndef ADDITIONALDBUSTYPES_HPP
#define ADDITIONALDBUSTYPES_HPP

#include <QtDBus>

struct UniverseStatus
{
    bool universe1;
    bool universe2;
};
Q_DECLARE_METATYPE(UniverseStatus);

inline QDBusArgument &operator<<(QDBusArgument &argument, const UniverseStatus &status)
{
    argument.beginStructure();
        argument << status.universe1 << status.universe2;
    argument.endStructure();
    return argument;
}

inline const QDBusArgument &operator>>(const QDBusArgument &argument, UniverseStatus &status)
{
    argument.beginStructure();
        argument >> status.universe1 >> status.universe2;
    argument.endStructure();
    return argument;
}

typedef QList<QByteArray> Frame;
Q_DECLARE_METATYPE(Frame);

inline void registerAdditionalTypes()
{
    qDBusRegisterMetaType<UniverseStatus>();
    qDBusRegisterMetaType<Frame>();
}

#endif

