/* 
 * kolamaEditor - Editor for simple animations on KoLaMa
 * 
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 * 
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#ifndef BOXMODEL_HPP
#define BOXMODEL_HPP

#include <QAbstractTableModel>
#include <QVector>
#include <QPoint>

class BoxModel : public QAbstractTableModel
{
    Q_OBJECT

    struct Box
    {
        Box();
        QPoint position;
        QString direction;
    };

public:
    BoxModel(QObject* parent = 0);
    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex& index) const;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);

private:
    QVector<Box> boxes_;
};

#endif

