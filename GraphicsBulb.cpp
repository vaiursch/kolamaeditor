/*
 * kolamaEditor - Editor for simple animations on KoLaMa
 *
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 *
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#include "GraphicsBulb.hpp"

#include "constants.hpp"

#include <QtGui>

GraphicsBulb::GraphicsBulb(int x, int y, QGraphicsItem* parent)
	: QGraphicsEllipseItem(x*PIXELFACTOR,y*PIXELFACTOR,PIXELFACTOR,PIXELFACTOR,parent)
{
	on_ = false;
	setPen(Qt::NoPen);
	setFlags(QGraphicsItem::ItemStacksBehindParent);
}

bool GraphicsBulb::getOn()
{
	return on_;
}

void GraphicsBulb::setOn(bool on)
{
	on_ = on;
	setBrush(Qt::red);
}

void GraphicsBulb::setOff(bool off)
{
	on_ = !off;
	setBrush(Qt::white);
}

void GraphicsBulb::toggle()
{
	if(on_) setOff();
	else setOn();
}
