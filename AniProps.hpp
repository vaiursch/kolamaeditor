/* 
 * kolamaEditor - Editor for simple animations on KoLaMa
 * 
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 * 
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#ifndef ANIPROPS_HPP
#define ANIPROPS_HPP

#include <QWidget>

class QLabel;
class QSpinBox;

class AniProps  : public QWidget
{
    Q_OBJECT

public:
    AniProps(QWidget* parent = 0);
    void setProps(int width, int height, bool universe1, bool universe2);
    int getWidth();
    int getHeight();

public slots:
    void lostUniverse(int i);
    void foundUniverse(int i);

signals:
    void framewidthChanged(int width);
    void frameheightChanged(int height);

private:
    QLabel* fwlabel_;
    QLabel* fhlabel_;
    QLabel* universe1label_;
    QLabel* universe2label_;

    QSpinBox* fwbox_;
    QSpinBox* fhbox_;

};

#endif
