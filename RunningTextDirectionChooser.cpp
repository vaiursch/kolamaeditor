/*
 * kolamaEditor - Editor for simple animations on KoLaMa
 *
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 *
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#include "RunningTextDirectionChooser.hpp"

#include <QtGui>

RunningTextDirectionChooser::RunningTextDirectionChooser(QWidget *parent) :
	QDialog(parent)
{
	buttonlabel_ = new QLabel(tr("Choose the direction of the running text:"));
	left_ = new QPushButton(tr("Left"));
	right_ = new QPushButton(tr("Right"));
	up_ = new QPushButton(tr("Up"));
	down_ = new QPushButton(tr("Down"));
	cancel_ = new QPushButton(tr("Cancel"));

	QVBoxLayout* layout = new QVBoxLayout();
	layout->addWidget(buttonlabel_);
	layout->addWidget(left_);
	layout->addWidget(right_);
	layout->addWidget(up_);
	layout->addWidget(down_);

	QFrame* line = new QFrame();
	line->setFrameShape(QFrame::HLine);
	line->setFrameShadow(QFrame::Sunken);
	layout->addWidget(line);

	layout->addWidget(cancel_);

	setLayout(layout);

	connect(cancel_, SIGNAL(clicked()), this, SLOT(reject()));
	connect(left_, SIGNAL(clicked()), this, SLOT(left()));
	connect(right_, SIGNAL(clicked()), this, SLOT(right()));
	connect(up_, SIGNAL(clicked()), this, SLOT(up()));
	connect(down_, SIGNAL(clicked()), this, SLOT(down()));
}

QString RunningTextDirectionChooser::getDirection()
{
	return direction_;
}

void RunningTextDirectionChooser::left()
{
	direction_ = "left";
	accept();
}

void RunningTextDirectionChooser::right()
{
	direction_ = "right";
	accept();
}

void RunningTextDirectionChooser::up()
{
	direction_ = "up";
	accept();
}

void RunningTextDirectionChooser::down()
{
	direction_ = "down";
	accept();
}
