/*
 * kolamaEditor - Editor for simple animations on KoLaMa
 *
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 *
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#include "WorkSpace.hpp"
#include "AniProps.hpp"
#include "DaemonInterface.hpp"
#include "BoxModel.hpp"
#include "SpinBoxDelegate.hpp"
#include "ComboBoxDelegate.hpp"
#include "GraphicsBoxGroup.hpp"
#include "GraphicsFrame.hpp"
#include "RunningTextDirectionChooser.hpp"

#include "constants.hpp"

#include <QtGui>

WorkSpace::WorkSpace(QWidget* parent)
	: QMainWindow(parent), frames_()
{
	daemon_ = 0; // daemon is created later

	// create all the widgets
	aniprops_ = new AniProps();

	boxlist_ = new QTableView();
	boxmodel_ = new BoxModel(this);
	boxlist_->setModel(boxmodel_);
	boxlist_->setSelectionMode(QAbstractItemView::NoSelection);
	boxlist_->verticalHeader()->setResizeMode(QHeaderView::Stretch);
	boxlist_->horizontalHeader()->setResizeMode(QHeaderView::Stretch);

	SpinBoxDelegate* spinbox = new SpinBoxDelegate(this);
	ComboBoxDelegate* combobox = new ComboBoxDelegate(this);
	boxlist_->setItemDelegateForColumn(X_COLUMN, spinbox);
	boxlist_->setItemDelegateForColumn(Y_COLUMN, spinbox);
	boxlist_->setItemDelegateForColumn(DIR_COLUMN, combobox);

	framescene_ = new QGraphicsScene(this);
	frameview_ = new QGraphicsView(framescene_);
	setCentralWidget(frameview_);

	boxgroup_ = new GraphicsBoxGroup(this);
	boxgroup_->setZValue(1.0);

	frames_.append(new GraphicsFrame());
	currentframeindex_ = 0;

	framescene_->addItem(boxgroup_);
	framescene_->addItem(frames_[0]);

	framepos_ = new QLabel("1/1");
	framepos_->setAlignment(Qt::AlignRight);

	fpsedit_ = new QSpinBox();
	fpsedit_->setMinimum(1);
	fpsedit_->setMaximum(400);

	// create all actions
	prevframeaction_ = new QAction(QIcon::fromTheme("go-previous", QIcon(":/icons/go-previous.png")), tr("Previous frame"), this);
	prevframeaction_->setEnabled(false);
	nextframeaction_ = new QAction(QIcon::fromTheme("go-next", QIcon(":/icons/go-next.png")), tr("Next frame"), this);
	nextframeaction_->setEnabled(false);
	newframeaction_ = new QAction(QIcon::fromTheme("document-new", QIcon(":/icons/document-new.png")), tr("New frame"), this);
	copyframeaction_ = new QAction(QIcon::fromTheme("edit-copy", QIcon(":/icons/edit-copy.png")), tr("Copy current frame"), this);
	deleteframeaction_ = new QAction(QIcon::fromTheme("edit-delete", QIcon(":/icons/edit-delete.png")), tr("Delete current frame"), this);
	deleteframeaction_->setEnabled(false);

	playpauseaction_ = new QAction(QIcon::fromTheme("media-playback-start", QIcon(":/icons/media-playback-start.png")), tr("Play"), this);
	playtimer_ = new QTimer(this);
	playtimer_->setInterval(1000);
	playing_ = false;

	savefileaction_ = new QAction(QIcon::fromTheme("document-save", QIcon(":/icons/document-save.png")), tr("&Save"), this);
	loadfileaction_ = new QAction(QIcon::fromTheme("document-open", QIcon(":/icons/document-open.png")), tr("&Load"), this);

	boxesvisibleaction_ = new QAction(QIcon(":/icons/grid.png"), tr("Boxes visible"), this);
	boxesvisibleaction_->setCheckable(true);
	boxesvisibleaction_->setChecked(true);

	runningtextwizardaction_ = new QAction(tr("Start \"&Running Text\" wizard"), this);
	runningtextwizardaction_->setShortcut(tr("Ctrl+W"));

	// add frametools to anitoolbar
	anitoolbar_ = addToolBar(tr("AniToolbar"));
	anitoolbar_->addAction(prevframeaction_);
	anitoolbar_->addAction(playpauseaction_);
	anitoolbar_->addAction(nextframeaction_);
	anitoolbar_->addAction(newframeaction_);
	anitoolbar_->addAction(copyframeaction_);
	anitoolbar_->addAction(deleteframeaction_);
	anitoolbar_->addAction(savefileaction_);
	anitoolbar_->addAction(loadfileaction_);
	anitoolbar_->addWidget(fpsedit_);
	anitoolbar_->addAction(boxesvisibleaction_);

	// create dockwidgets
	QDockWidget* anipropsdock = new QDockWidget(tr("Animation Properties"), this);
	anipropsdock->setWidget(aniprops_);
	addDockWidget(Qt::RightDockWidgetArea, anipropsdock);

	QDockWidget* boxlistdock = new QDockWidget(tr("Box List"), this);
	boxlistdock->setWidget(boxlist_);
	addDockWidget(Qt::RightDockWidgetArea, boxlistdock);

	// statusbar
	statusBar()->addPermanentWidget(framepos_);
	statusBar()->setSizeGripEnabled(false);

	// menubar
	QMenu* fileMenu = menuBar()->addMenu(tr("&File"));
	fileMenu->addAction(loadfileaction_);
	fileMenu->addAction(savefileaction_);
	fileMenu->addAction(runningtextwizardaction_);
	fileMenu->addSeparator();
	fileMenu->addAction(tr("&Quit"), this, SLOT(close()));

	QMenu* viewMenu = menuBar()->addMenu(tr("&View"));
	viewMenu->addAction(boxesvisibleaction_);

	QMenu* aniMenu = menuBar()->addMenu(tr("&Animation"));
	aniMenu->addAction(playpauseaction_);
	aniMenu->addAction(nextframeaction_);
	aniMenu->addAction(prevframeaction_);
	aniMenu->addSeparator();
	aniMenu->addAction(newframeaction_);
	aniMenu->addAction(deleteframeaction_);
	aniMenu->addAction(copyframeaction_);

	// connections, independent of dbus status
	connect(boxmodel_, SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)), boxgroup_, SLOT(updateBox(const QModelIndex&)));
	connect(boxesvisibleaction_, SIGNAL(toggled(bool)), boxgroup_, SLOT(setBoxesVisible(bool)));
	connect(aniprops_, SIGNAL(framewidthChanged(int)), frames_[0], SLOT(setWidth(int)));
	connect(aniprops_, SIGNAL(frameheightChanged(int)), frames_[0], SLOT(setHeight(int)));
	connect(prevframeaction_, SIGNAL(triggered()), this, SLOT(prevFrame()));
	connect(nextframeaction_, SIGNAL(triggered()), this, SLOT(nextFrame()));
	connect(newframeaction_, SIGNAL(triggered()), this, SLOT(newFrame()));
	connect(copyframeaction_, SIGNAL(triggered()), this, SLOT(copyFrame()));
	connect(deleteframeaction_, SIGNAL(triggered()), this, SLOT(deleteFrame()));
	connect(this, SIGNAL(frameChanged(int)), this, SLOT(updateActions()));
	connect(this, SIGNAL(frameChanged(int)), this, SLOT(updateFramepos(int)));
	connect(fpsedit_, SIGNAL(valueChanged(int)), this, SLOT(updatePlaytimer(int)));
	connect(playpauseaction_, SIGNAL(triggered()), this, SLOT(playpause()));
	connect(savefileaction_, SIGNAL(triggered()), this, SLOT(saveFile()));
	connect(loadfileaction_, SIGNAL(triggered()), this, SLOT(loadFile()));
	connect(runningtextwizardaction_, SIGNAL(triggered()), this, SLOT(createRunningText()));
}

bool WorkSpace::dbussetup()
{
	if(daemon_ != 0)
		return true;

	daemon_ = new DaemonInterface("kosta.kolama", "/daemon", QDBusConnection::sessionBus(), this);
	if(daemon_->lastError().isValid()) {
		QMessageBox::critical(this, tr("Could not connect to the daemon"),
							  tr("The editor could not connect to the daemon. "
								  "This usually means, that either kolamad or "
								  "the D-Bus session bus daemon are not running.\n"
								  "If both run, please include this message in "
								  "your bug report:\n %1").arg(daemon_->lastError().message()));
		return false;
	}

	if(daemon_->version() != DAEMON_INTERFACE_VERSION) {
		QMessageBox::critical(this, tr("Wrong interface version"),
							  tr("The kolamad running on the D-Bus session Bus "
								 "has the wrong interface version. Please "
								 "update the daemon and/or the editor.\n"
								 "Expected version %1, got version %2").arg(DAEMON_INTERFACE_VERSION).arg(daemon_->version()));
		return false;
	}

	UniverseStatus ustatus = daemon_->universes();
	aniprops_->setProps(daemon_->framewidth(), daemon_->frameheight(), ustatus.universe1, ustatus.universe2);

	// connections, dependent on dbus status
	connect(daemon_, SIGNAL(foundUniverse(int)), aniprops_, SLOT(foundUniverse(int)));
	connect(daemon_, SIGNAL(lostUniverse(int)), aniprops_, SLOT(lostUniverse(int)));
	connect(boxmodel_, SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(sendBoxData(const QModelIndex&)));
	connect(aniprops_, SIGNAL(framewidthChanged(int)), this, SLOT(sendFramewidth(int)));
	connect(aniprops_, SIGNAL(frameheightChanged(int)), this, SLOT(sendFrameheight(int)));
	connect(playtimer_, SIGNAL(timeout()), this, SLOT(sendFrame()));

	return true;
}

void WorkSpace::sendBoxData(const QModelIndex& index)
{
	int number = index.row() + 1;
	int x = boxmodel_->data(boxmodel_->index(index.row(), X_COLUMN)).toInt();
	int y = boxmodel_->data(boxmodel_->index(index.row(), Y_COLUMN)).toInt();
	QString direction = boxmodel_->data(boxmodel_->index(index.row(), DIR_COLUMN)).toString();

	daemon_->setBox(number, x, y, direction);
}

void WorkSpace::sendFramewidth(int width)
{
	daemon_->setFramewidth(width);
}

void WorkSpace::sendFrameheight(int height)
{
	daemon_->setFrameheight(height);
}

void WorkSpace::sendFrame()
{
	framescene_->removeItem(frames_[currentframeindex_]);
	currentframeindex_++;
	if(currentframeindex_ == frames_.size())
		currentframeindex_ = 0;
	framescene_->addItem(frames_[currentframeindex_]);
	daemon_->setFrame(frames_[currentframeindex_]->getFrame());

	updateFramepos(currentframeindex_);
}

void WorkSpace::prevFrame()
{
	framescene_->removeItem(frames_[currentframeindex_]);
	currentframeindex_--;
	if(currentframeindex_ < 0) {
		currentframeindex_ = frames_.size() - 1;
	}
	framescene_->addItem(frames_[currentframeindex_]);

	emit frameChanged(currentframeindex_);
}

void WorkSpace::nextFrame()
{
	framescene_->removeItem(frames_[currentframeindex_]);
	currentframeindex_++;
	if(currentframeindex_ > frames_.size() - 1) {
		currentframeindex_ = 0;
	}
	framescene_->addItem(frames_[currentframeindex_]);

	emit frameChanged(currentframeindex_);
}

void WorkSpace::newFrame()
{
	GraphicsFrame* newframe = new GraphicsFrame(aniprops_->getWidth(), aniprops_->getHeight());
	connect(aniprops_, SIGNAL(framewidthChanged(int)), newframe, SLOT(setWidth(int)));
	connect(aniprops_, SIGNAL(frameheightChanged(int)), newframe, SLOT(setHeight(int)));

	frames_.insert(currentframeindex_+1, newframe);
	nextFrame();

	emit frameChanged(currentframeindex_);
}

void WorkSpace::copyFrame()
{
	newFrame();
	frames_[currentframeindex_]->setFrame(frames_[currentframeindex_-1]->getFrame());

	emit frameChanged(currentframeindex_);
}

void WorkSpace::deleteFrame()
{
	int oldframeindex;
	if(currentframeindex_ == 0) {
		nextFrame();
		oldframeindex = currentframeindex_ - 1;
		currentframeindex_--;
	} else {
		prevFrame();
		oldframeindex = currentframeindex_ + 1;
	}

	disconnect(aniprops_, SIGNAL(framewidthChanged(int)), frames_[oldframeindex], SLOT(setWidth(int)));
	disconnect(aniprops_, SIGNAL(frameheightChanged(int)), frames_[oldframeindex], SLOT(setHeight(int)));
	delete frames_[oldframeindex];
	frames_.removeAt(oldframeindex);

	emit frameChanged(currentframeindex_);
}

void WorkSpace::playpause()
{
	if(playing_) {
		playtimer_->stop();
		playpauseaction_->setIcon(QIcon::fromTheme("media-playback-start", QIcon(":/icons/media-playback-start.png")));
		playing_ = false;

		updateActions();
		newframeaction_->setEnabled(true);
		savefileaction_->setEnabled(true);
		loadfileaction_->setEnabled(true);
		copyframeaction_->setEnabled(true);
	} else {
		playtimer_->start();
		playpauseaction_->setIcon(QIcon::fromTheme("media-playback-pause", QIcon(":/icons/media-playback-pause.png")));
		playing_ = true;
		daemon_->setFrame(frames_[currentframeindex_]->getFrame());

		prevframeaction_->setEnabled(false);
		nextframeaction_->setEnabled(false);
		deleteframeaction_->setEnabled(false);
		copyframeaction_->setEnabled(false);
		newframeaction_->setEnabled(false);
		savefileaction_->setEnabled(false);
		loadfileaction_->setEnabled(false);
	}
}

void WorkSpace::saveFile()
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Select location to save animation"));
	if(dir == "")
		return;

	QDir directory(dir);
	QString file;
	foreach(file, directory.entryList()) {
		directory.remove(file);
	}

	QSettings config(dir + "/config", QSettings::IniFormat);
	config.setValue("version", 1);
	config.setValue("fps", fpsedit_->value());
	config.setValue("frameheight", aniprops_->getHeight());
	config.setValue("framewidth", aniprops_->getWidth());
	config.setValue("boxesvisible", boxesvisibleaction_->isChecked());

	for(int i = 0; i < 8; i++) {
		QModelIndex xindex = boxmodel_->index(i, X_COLUMN);
		QModelIndex yindex = boxmodel_->index(i, Y_COLUMN);
		QModelIndex dirindex = boxmodel_->index(i, DIR_COLUMN);

		QString box = "box%1";
		box = box.arg(i+1);
		config.setValue(box + "/x", boxmodel_->data(xindex));
		config.setValue(box + "/y", boxmodel_->data(yindex));
		config.setValue(box + "/dir", boxmodel_->data(dirindex));
	}

	for(int i = 0; i < frames_.size(); i++) {
		QString filename = dir + "/%1";
		QFile file(filename.arg(i+1));
		file.open(QIODevice::WriteOnly | QIODevice::Text);
		QTextStream stream(&file);

		Frame frame = frames_[i]->getFrame();
		for(int r = 0; r < aniprops_->getWidth(); r++) {
			for(int s = 0; s < aniprops_->getHeight(); s++) {
				stream << "0x" << QString::number((unsigned char)frame[r][s], 16);
				if((unsigned char)frame[r][s] == 0) {
					stream << "0 ";
				} else {
					stream << ' ';
				}
			}
			stream << '\n';
		}
		file.close();
	}
}

void WorkSpace::loadFile()
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Select location to load animation from"));
	if(dir == "")
		return;

	if(!QFile::exists(dir + "/config"))
		return;

	QSettings config(dir + "/config", QSettings::IniFormat);
	if(config.value("version").toInt() != 1)
		return;

	fpsedit_->setValue(config.value("fps").toInt());

	UniverseStatus ustatus = daemon_->universes();
	int framewidth = config.value("framewidth").toInt();
	int frameheight = config.value("frameheight").toInt();
	aniprops_->setProps(framewidth, frameheight, ustatus.universe1, ustatus.universe2);
	daemon_->setFramewidth(framewidth);
	daemon_->setFrameheight(frameheight);

	boxesvisibleaction_->setChecked(config.value("boxesvisible").toBool());

	for(int i = 0; i < 8; i++) {
		QModelIndex xindex = boxmodel_->index(i, X_COLUMN);
		QModelIndex yindex = boxmodel_->index(i, Y_COLUMN);
		QModelIndex dirindex = boxmodel_->index(i, DIR_COLUMN);

		QString box = QString("box%1").arg(i+1);

		boxmodel_->setData(xindex, config.value(box + "/x"));
		boxmodel_->setData(yindex, config.value(box + "/y"));
		boxmodel_->setData(dirindex, config.value(box + "/dir"));
	}

	framescene_->removeItem(frames_[currentframeindex_]);
	while(frames_.size() > 0) {
		disconnect(aniprops_, SIGNAL(framewidthChanged(int)), frames_.first(), SLOT(setWidth(int)));
		disconnect(aniprops_, SIGNAL(frameheightChanged(int)), frames_.first(), SLOT(setHeight(int)));
		delete frames_.first();
		frames_.removeFirst();
	}

	int numframes = QDir(dir).entryList().size() - 3;
	for(int i = 0; i < numframes; i++) {
		QString filename = dir + "/%1";
		QFile file(filename.arg(i+1));
		file.open(QIODevice::ReadOnly | QIODevice::Text);
		QTextStream stream(&file);

		GraphicsFrame* newframe = new GraphicsFrame(aniprops_->getWidth(), aniprops_->getHeight());
		connect(aniprops_, SIGNAL(framewidthChanged(int)), newframe, SLOT(setWidth(int)));
		connect(aniprops_, SIGNAL(frameheightChanged(int)), newframe, SLOT(setHeight(int)));
		frames_.append(newframe);

		Frame frame;
		for(int i = 0; i < framewidth; i++) {
			QByteArray col(frameheight, 0);
			for(int j = 0; j < frameheight; j++) {
				unsigned short c;
				stream >> c;
				col[j] = c;
			}
			frame.append(col);
		}
		frames_.last()->setFrame(frame);

		file.close();
	}

	currentframeindex_ = 0;
	framescene_->addItem(frames_[currentframeindex_]);
	emit frameChanged(currentframeindex_);
}

void WorkSpace::updateActions()
{
	if(frames_.size() == 1) {
		deleteframeaction_->setEnabled(false);
		prevframeaction_->setEnabled(false);
		nextframeaction_->setEnabled(false);
	} else {
		deleteframeaction_->setEnabled(true);
		nextframeaction_->setEnabled(true);
		prevframeaction_->setEnabled(true);
	}
}

void WorkSpace::updateFramepos(int index)
{
	framepos_->setText(QString("%1/%2").arg(index+1).arg(frames_.size()));
}

void WorkSpace::updatePlaytimer(int value)
{
	playtimer_->setInterval(1000.0/value);
}

void WorkSpace::createRunningText()
{
	RunningTextDirectionChooser dirchooser;
	int code = dirchooser.exec();
	if(code == QDialog::Rejected) {
		return;
	}

	if(frames_.size() > 1) {
		QMessageBox::StandardButton button = QMessageBox::warning(this, tr("Warning: Frames will be deleted"),
																  tr("You have more than one frame. All frames, except the first, will be deleted."),
																  QMessageBox::Ok | QMessageBox::Cancel);
		if(button == QMessageBox::Cancel) {
			return;
		}

		if(currentframeindex_ != 0) {
			framescene_->removeItem(frames_[currentframeindex_]);
			currentframeindex_ = 0;
			framescene_->addItem(frames_[currentframeindex_]);
		}

		while(frames_.size() > 1) {
			disconnect(aniprops_, SIGNAL(framewidthChanged(int)), frames_.last(), SLOT(setWidth(int)));
			disconnect(aniprops_, SIGNAL(frameheightChanged(int)), frames_.last(), SLOT(setHeight(int)));
			delete frames_.last();
			frames_.removeLast();
		}

		emit frameChanged(currentframeindex_);
	}

	if(dirchooser.getDirection() == "left") {
		for(int i = 1; i < aniprops_->getWidth(); i++) {
			copyFrame();
			Frame frame = frames_[currentframeindex_]->getFrame();
			frame.push_back(frame.first());
			frame.pop_front();
			frames_[currentframeindex_]->setFrame(frame);
		}
	}

	if(dirchooser.getDirection() == "right") {
		for(int i = 1; i < aniprops_->getWidth(); i++) {
			copyFrame();
			Frame frame = frames_[currentframeindex_]->getFrame();
			frame.push_front(frame.last());
			frame.pop_back();
			frames_[currentframeindex_]->setFrame(frame);
		}
	}

	if(dirchooser.getDirection() == "up") {
		for(int i = 1; i < aniprops_->getHeight(); i++) {
			copyFrame();
			Frame frame = frames_[currentframeindex_]->getFrame();
			for(int j = 0; j < frame.size(); j++) {
				frame[j].append(frame[j][0]);
				frame[j] = frame[j].right(frame[j].size()-1);
			}
			frames_[currentframeindex_]->setFrame(frame);
		}
	}

	if(dirchooser.getDirection() == "down") {
		for(int i = 1; i < aniprops_->getHeight(); i++) {
			copyFrame();
			Frame frame = frames_[currentframeindex_]->getFrame();
			for(int j = 0; j < frame.size(); j++) {
				frame[j].prepend(frame[j][frame[j].size()-1]);
				frame[j].chop(1);
			}
			frames_[currentframeindex_]->setFrame(frame);
		}
	}

	nextFrame();
}
