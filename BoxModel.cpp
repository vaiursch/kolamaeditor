/* 
 * kolamaEditor - Editor for simple animations on KoLaMa
 * 
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 * 
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#include "BoxModel.hpp"

#include "constants.hpp"

#include <QtGui>

BoxModel::Box::Box()
    : position(15, 0)
{
    direction = "down";
}

BoxModel::BoxModel(QObject* parent)
    : QAbstractTableModel(parent), boxes_(8)
{
}

int BoxModel::rowCount(const QModelIndex& parent) const
{
    if(parent.isValid())
        return 0;
    else
        return 8;
}

int BoxModel::columnCount(const QModelIndex& parent) const
{
    if(parent.isValid())
        return 0;
    else
        return 3;
}

QVariant BoxModel::data(const QModelIndex& index, int role) const
{
    if(!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole || role == Qt::EditRole) {
        switch(index.column()) {
            case X_COLUMN:
                return boxes_[index.row()].position.x();

            case Y_COLUMN:
                return boxes_[index.row()].position.y();

            case DIR_COLUMN:
                return boxes_[index.row()].direction;

            default:
                return QVariant();
        }
    }

    return QVariant();
}

QVariant BoxModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole) {
        if(orientation == Qt::Vertical) {
            return section+1;
        }

        if(orientation == Qt::Horizontal) {
            switch(section) {
                case X_COLUMN:
                    return QString(tr("x"));

                case Y_COLUMN:
                    return QString(tr("y"));

                case DIR_COLUMN:
                    return QString(tr("->"));

                default:
                    return QVariant();
            }
        }
    }

    return QVariant();
}

Qt::ItemFlags BoxModel::flags(const QModelIndex& index) const
{
    if(!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

bool BoxModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if(!index.isValid())
        return false;

    if(role == Qt::EditRole) {
        if(index.column() == DIR_COLUMN) {
            QString newdir;

            if(value.toString() == tr("down"))
                newdir = "down";

            if(value.toString() == tr("right"))
                newdir = "right";

            if(value.toString() == tr("up"))
                newdir = "up";

            if(value.toString() == tr("left"))
                newdir = "left";

            if(newdir != "") {
                boxes_[index.row()].direction = newdir;
                emit dataChanged(index, index);
                return true;
            }
            return false;
        }

        if(!value.canConvert<int>())
            return false;

        switch(index.column()) {
            case X_COLUMN:
                boxes_[index.row()].position.setX(value.toInt());
                break;

            case Y_COLUMN:
                boxes_[index.row()].position.setY(value.toInt());
                break;

            default:
                return false;
        }
        emit dataChanged(index, index);
        return true;
    }

    return false;
}
