/*
 * kolamaEditor - Editor for simple animations on KoLaMa
 *
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 *
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#ifndef WORKSPACE_HPP
#define WORKSPACE_HPP

#include <QMainWindow>
#include <QModelIndex>

class QTableView;
class QGraphicsView;
class QGraphicsScene;
class QCheckBox;
class QToolBar;
class QAction;
class QLabel;
class QSpinBox;
class QTimer;
class AniProps;
class DaemonInterface;
class BoxModel;
class GraphicsBoxGroup;
class GraphicsFrame;

class WorkSpace : public QMainWindow
{
	Q_OBJECT

public:
	WorkSpace(QWidget* parent = 0);
	bool dbussetup();

public slots:
	void sendBoxData(const QModelIndex& index);
	void sendFramewidth(int width);
	void sendFrameheight(int height);
	void sendFrame();

	void prevFrame();
	void nextFrame();
	void newFrame();
	void copyFrame();
	void deleteFrame();
	void playpause();

	void saveFile();
	void loadFile();

private:
	// Widgets
	AniProps* aniprops_;
	QTableView* boxlist_;
	BoxModel* boxmodel_;
	QGraphicsView* frameview_;
	QGraphicsScene* framescene_;
	GraphicsBoxGroup* boxgroup_;
	QToolBar* anitoolbar_;
	QLabel* framepos_;
	QSpinBox* fpsedit_;

	int currentframeindex_;
	QList<GraphicsFrame*> frames_;
	QTimer* playtimer_;
	bool playing_;

	// Actions
	QAction* prevframeaction_;
	QAction* nextframeaction_;
	QAction* newframeaction_;
	QAction* copyframeaction_;
	QAction* deleteframeaction_;
	QAction* playpauseaction_;
	QAction* savefileaction_;
	QAction* loadfileaction_;
	QAction* boxesvisibleaction_;
	QAction* runningtextwizardaction_;

	DaemonInterface* daemon_;
	static const uint DAEMON_INTERFACE_VERSION = 1;

private slots:
	void updateActions();
	void updateFramepos(int index);
	void updatePlaytimer(int value);
	void createRunningText();

signals:
	void frameChanged(int newframe);
};

#endif

