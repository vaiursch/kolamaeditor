# qmake Projektdatei für kolamaEditor
# Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>

TEMPLATE = app
TARGET = kolamaEditor
QT += dbus

RESOURCES += icons.qrc

# Input
HEADERS += WorkSpace.hpp \
		   AniProps.hpp \
		   DaemonInterface.hpp \
		   BoxModel.hpp \
		   SpinBoxDelegate.hpp \
		   ComboBoxDelegate.hpp \
		   GraphicsBoxGroup.hpp \
		   GraphicsBulb.hpp \
		   GraphicsFrame.hpp \
		   RunningTextDirectionChooser.hpp \
		   constants.hpp

SOURCES += WorkSpace.cpp \
		   AniProps.cpp \
		   DaemonInterface.cpp \
		   BoxModel.cpp \
		   SpinBoxDelegate.cpp \
		   ComboBoxDelegate.cpp \
		   GraphicsBoxGroup.cpp \
		   GraphicsBulb.cpp \
		   GraphicsFrame.cpp \
		   main.cpp \
		   RunningTextDirectionChooser.cpp

