/*
 * kolamaEditor - Editor for simple animations on KoLaMa
 *
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 *
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#include "GraphicsBoxGroup.hpp"

#include "constants.hpp"

#include <QtGui>

GraphicsBoxGroup::GraphicsBoxGroup(QObject* objparent, QGraphicsItem* itemparent)
    : QObject(objparent), QGraphicsItemGroup(itemparent), boxes_(8)
{
    for(int i = 0; i < 8; i++) {
        boxes_[i] = new QGraphicsRectItem(0.0,0.0, 16.0*PIXELFACTOR, 8.0*PIXELFACTOR);

        QPen pen = boxes_[i]->pen();
        pen.setWidth(1);

        QVector<qreal> pattern;
        pattern << 5 << 35;
        pen.setDashPattern(pattern);
        pen.setDashOffset(i*5);
        pen.setStyle(Qt::CustomDashLine);
        pen.setColor(QColor(Qt::GlobalColor(i+7)));
        boxes_[i]->setPen(pen);

        boxes_[i]->setTransformOriginPoint(15.5*PIXELFACTOR,0.5*PIXELFACTOR);

        addToGroup(boxes_[i]);
    }
}

void GraphicsBoxGroup::updateBox(const QModelIndex& index)
{
    switch(index.column()) {
        case X_COLUMN:
            boxes_[index.row()]->setX((index.data().toReal()-15.0)*PIXELFACTOR);
            break;

        case Y_COLUMN:
            boxes_[index.row()]->setY((index.data().toReal())*PIXELFACTOR);
            break;

        case DIR_COLUMN:
            if(index.data().toString() == "down") {
                boxes_[index.row()]->setRotation(0);
            }

            if(index.data().toString() == "right") {
                boxes_[index.row()]->setRotation(270);
            }

            if(index.data().toString() == "up") {
                boxes_[index.row()]->setRotation(180);
            }

            if(index.data().toString() == "left") {
                boxes_[index.row()]->setRotation(90);
            }
            break;
    }
}

void GraphicsBoxGroup::setBoxesVisible(bool checked)
{
    setVisible(checked);
}