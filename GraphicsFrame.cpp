/*
 * kolamaEditor - Editor for simple animations on KoLaMa
 *
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 *
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#include "GraphicsFrame.hpp"
#include "GraphicsBulb.hpp"

#include "constants.hpp"

#include <QtGui>

GraphicsFrame::GraphicsFrame(int width, int height, QGraphicsItem* parent)
    : QObject(0), QGraphicsRectItem(0,0,width*PIXELFACTOR, height*PIXELFACTOR, parent),
      bulbs_(width, QVector<GraphicsBulb*>(height))
{
    setAcceptedMouseButtons(Qt::LeftButton | Qt::RightButton);

    QPen pen = this->pen();
    pen.setWidth(2);
    setPen(pen);

    for(int i = 0; i < width; i++) {
        for(int j = 0; j < height; j++) {
            bulbs_[i][j] = new GraphicsBulb(i,j, this);
        }
    }
}

Frame GraphicsFrame::getFrame()
{
    int width = rect().width() / PIXELFACTOR;
    int height = rect().height() / PIXELFACTOR;

    Frame frame;

    for(int i = 0; i < width; i++) {
        QByteArray col(height, 0);
        for(int j = 0; j < height; j++) {
            if(bulbs_[i][j]->getOn())
                col[j] = 0xFF;
        }
        frame.append(col);
    }

    return frame;
}

void GraphicsFrame::setFrame(Frame frame)
{
    int width = rect().width() / PIXELFACTOR;
    int height = rect().height() / PIXELFACTOR;

    for(int i = 0; i < width; i++) {
        for(int j = 0; j < height; j ++) {
            if((unsigned short)frame[i][j] > 0x88)
                bulbs_[i][j]->setOn();
            else
                bulbs_[i][j]->setOff();
        }
    }
}

void GraphicsFrame::setWidth(int width)
{
    int oldwidth = rect().width() / PIXELFACTOR;
    int oldheight = rect().height() / PIXELFACTOR;

    if(width > oldwidth) {
        while(bulbs_.size() != width) {
            bulbs_.append(QVector<GraphicsBulb*>(oldheight));
            for(int j = 0; j < oldheight; j++) {
                bulbs_.last()[j] = new GraphicsBulb(bulbs_.size()-1,j, this);
            }
        }
    } else if(width < oldwidth) {
        while(bulbs_.size() != width) {
            for(int j = 0; j < oldheight; j++) {
                delete bulbs_.last()[j];
            }
            bulbs_.erase(bulbs_.end()-1);
        }
    }

    setRect(0,0, width*PIXELFACTOR, rect().height());
}

void GraphicsFrame::setHeight(int height)
{
    int oldheight = rect().height() / PIXELFACTOR;

    if(height > oldheight) {
        for(int i = 0; i < bulbs_.size(); i++) {
            while(bulbs_[i].size() != height) {
                bulbs_[i].append(new GraphicsBulb(i,bulbs_[i].size(), this));
            }
        }
    } else if(height < oldheight) {
        for(int i = 0; i < bulbs_.size(); i++) {
            while(bulbs_[i].size() != height) {
                delete bulbs_[i].last();
                bulbs_[i].erase(bulbs_[i].end()-1);
            }
        }
    }

    setRect(0,0, rect().width(), height*PIXELFACTOR);
}

void GraphicsFrame::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    int i = event->pos().x() / PIXELFACTOR;
    int j = event->pos().y() / PIXELFACTOR;

    if(event->button() == Qt::LeftButton) {
        bulbs_[i][j]->setOn();
    } else if(event->button() == Qt::RightButton) {
        bulbs_[i][j]->setOff();
    }
    event->accept();
}

void GraphicsFrame::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    int i = event->pos().x() / PIXELFACTOR;
    int j = event->pos().y() / PIXELFACTOR;

    if(i >= 0 && i < bulbs_.size() && j >= 0 && j < bulbs_[i].size()) {
        if(event->buttons() == Qt::LeftButton) {
            bulbs_[i][j]->setOn();
        } else if(event->buttons() == Qt::RightButton) {
            bulbs_[i][j]->setOff();
        }
    }
}

void GraphicsFrame::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    event->accept();
}