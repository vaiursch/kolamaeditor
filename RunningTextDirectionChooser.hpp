/*
 * kolamaEditor - Editor for simple animations on KoLaMa
 *
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 *
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#ifndef RUNNINGTEXTDIRECTIONCHOOSER_HPP
#define RUNNINGTEXTDIRECTIONCHOOSER_HPP

#include <QDialog>

class QLabel;
class QPushButton;

class RunningTextDirectionChooser : public QDialog
{
	Q_OBJECT

public:
	RunningTextDirectionChooser(QWidget *parent = 0);
	QString getDirection();

private:
	QLabel* buttonlabel_;
	QPushButton* left_;
	QPushButton* right_;
	QPushButton* up_;
	QPushButton* down_;
	QPushButton* cancel_;

	QString direction_;

private slots:
	void left();
	void right();
	void up();
	void down();
};

#endif
