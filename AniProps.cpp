/* 
 * kolamaEditor - Editor for simple animations on KoLaMa
 * 
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 * 
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamaEditor. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#include "AniProps.hpp"

#include <QtGui>
#include <limits>

AniProps::AniProps(QWidget* parent)
    : QWidget(parent)
{
    fwlabel_ = new QLabel(tr("Framewidth:"));
    fhlabel_ = new QLabel(tr("Frameheight:"));
    universe1label_ = new QLabel(tr("Universe 1 not available"));
    universe2label_ = new QLabel(tr("Universe 2 not available"));

    fwbox_ = new QSpinBox();
    fwbox_->setMinimum(1);
    fwbox_->setMaximum(std::numeric_limits<int>::max());
    fhbox_ = new QSpinBox();
    fhbox_->setMinimum(1);
    fhbox_->setMaximum(std::numeric_limits<int>::max());

    QGridLayout* layout = new QGridLayout();
    layout->addWidget(fwlabel_, 0, 0);
    layout->addWidget(fwbox_, 0, 1);
    layout->addWidget(fhlabel_, 1, 0);
    layout->addWidget(fhbox_, 1, 1);
    layout->addWidget(universe1label_, 4, 0, 1, 2);
    layout->addWidget(universe2label_, 5, 0, 1, 2);

    setLayout(layout);

    connect(fwbox_, SIGNAL(valueChanged(int)), this, SIGNAL(framewidthChanged(int)));
    connect(fhbox_, SIGNAL(valueChanged(int)), this, SIGNAL(frameheightChanged(int)));
}

void AniProps::setProps(int width, int height, bool universe1, bool universe2)
{
    fwbox_->setValue(width);
    fhbox_->setValue(height);

    if(universe1)
        universe1label_->setText(tr("Universe 1 available"));
    else
        universe1label_->setText(tr("Universe 1 not available"));

    if(universe2)
        universe2label_->setText(tr("Universe 2 available"));
    else
        universe2label_->setText(tr("Universe 2 not available"));
}

void AniProps::lostUniverse(int i)
{
    if(i == 1)
        universe1label_->setText(tr("Universe 1 not available"));
    if(i == 2)
        universe2label_->setText(tr("Universe 2 not available"));
}

void AniProps::foundUniverse(int i)
{
    if(i == 1)
        universe1label_->setText(tr("Universe 1 available"));
    if(i == 2)
        universe2label_->setText(tr("Universe 2 available"));
}

int AniProps::getWidth()
{
    return fwbox_->value();
}

int AniProps::getHeight()
{
    return fhbox_->value();
}
