/* 
 * kolamaEditor - Editor for simple animations on KoLaMa
 * 
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 * 
 * kolamaEditor ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * kolamaEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamad. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#ifndef DAEMONINTERFACE_HPP
#define DAEMONINTERFACE_HPP

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>

#include "AdditionalDBusTypes.hpp"

/*
 * Proxy class for interface kosta.kolama.daemonInterface
 */
class DaemonInterface : public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "kosta.kolama.daemonInterface"; }

    DaemonInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = 0);
    ~DaemonInterface();

    Q_PROPERTY(uint frameheight READ frameheight WRITE setFrameheight)
    inline uint frameheight() const
    { return qvariant_cast< uint >(property("frameheight")); }
    inline void setFrameheight(uint value)
    { setProperty("frameheight", qVariantFromValue(value)); }

    Q_PROPERTY(uint framewidth READ framewidth WRITE setFramewidth)
    inline uint framewidth() const
    { return qvariant_cast< uint >(property("framewidth")); }
    inline void setFramewidth(uint value)
    { setProperty("framewidth", qVariantFromValue(value)); }

    Q_PROPERTY(UniverseStatus universes READ universes)
    inline UniverseStatus universes() const
    { return qvariant_cast< UniverseStatus >(property("universes")); }

    Q_PROPERTY(uint version READ version)
    inline uint version() const
    { return qvariant_cast< uint >(property("version")); }

public slots: // METHODS
    inline QDBusReply<bool> setBox(int number, int x, int y, const QString &direction)
    {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(number) << qVariantFromValue(x) << qVariantFromValue(y) << qVariantFromValue(direction);
        return callWithArgumentList(QDBus::Block, "setBox", argumentList);
    }

    inline QDBusReply<bool> setFrame(Frame frame)
    {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(frame);
        return callWithArgumentList(QDBus::BlockWithGui, "setFrame", argumentList);
    }

signals: // SIGNALS
    void foundUniverse(int number);
    void lostUniverse(int number);
};

#endif
